import { html, css, LitElement } from 'lit-element'

export class eContact extends LitElement {
  static get styles() {
    return css`
      div {
        border: 1px solid black;
        padding: 10px;
        border-radius: 5px;
        display: inline-block;
      }
      h1 {
        font-size: 1.2rem;
        font-weight: normal
      }
    `;
  }


  static get properties() {
    return {
      name: { type: String },
      email: { type: String },
      seeMore: { type: Boolean },
      option: { type: String }
    }
  };

  constructor() {
    super();
    this.name = '';
    this.email = '';
    this.seeMore = false;
    this.option = 'Ver Mas';
  }

  toggle() {
    this.seeMore = !this.seeMore;
    this.seeMore ? this.option = 'Ver menos' : this.option = 'Ver Mas'
  }

  dataHandler() {
    let data = 'Informacion lanzada desde el evento'
    let object = {
      nombre: 'Abiel',
      edad: 25,
      nacionalidad: 'Mexicana'
    }

    this.dispatchEvent(new CustomEvent('my-event', {
      bubbles: true,
      composed: true,
      detail: object
    }));
  }

  render() {
    return html`
    <div>
      <h1>${this.name}</h1>
      <p><a href="#" @click="${this.toggle}">${this.option}</a></p>

      ${this.seeMore ? html`Email: ${this.email}` : ''}
    </div>

    <button @click="${this.dataHandler}">Button</button>
    `;
  }
}

customElements.define('e-contact', eContact);